package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // create a post function
    public void createPost(String stringToken, Post post) {
        // findByUsername to retrieve the user
        // Criteria for finding the user is from the jwtToken method getUsernameFromToken.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        // the title and content will come from the reqbody which is passed thru the post identifier.
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        // author retrieve from the token
        newPost.setUser(author);
        // the actual saving of post in our table
        postRepository.save(newPost);
    }

    // getting all posts
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }
    // note: in ResponseEntity<>, optional to put the type (T) inside the <>
    // edit a post
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        Post postForUpdating = postRepository.findById(id).get();

        String postAuthor = postForUpdating.getUser().getUsername();

        // we will retrieve the username of the currently logged-in user from his token
        // Request header Auth
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        // this will check if the logged-in user is the owner of the post
        if(authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully!", HttpStatus.OK);
        }
        // if the user from the token didnt match with the user from the post, we will warn them
        else{
            return new ResponseEntity<>("You are not allowed to edit this post", HttpStatus.UNAUTHORIZED);
        }
    }


    // delete a post
    public ResponseEntity deletePost(Long id, String stringToken) {
        // retrieve the id of the post that we want to delete
        Post postForDeletion = postRepository.findById(id).get();
        String postAuthor = postForDeletion.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("You have deleted your post", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You're not authorized to delete this post!", HttpStatus.UNAUTHORIZED);
        }
    }

    // Retrieve all user's posts
    @Override
    public  Iterable<Post> getMyPosts(String stringToken) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
            return (Iterable<Post>) postRepository.findByUser(user);
    }
}
