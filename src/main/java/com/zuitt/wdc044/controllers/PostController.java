package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.JwtResponse;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    @RequestMapping(value="/posts", method= RequestMethod.POST)
    //"ResponseEntity represent the whole HTTP response: status code,headers, and body.

    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post) {
        // We can access the "postService methods and pass the ff args:
            // stringToken of the current session will be retrieved from the request headers.
            // a "post" object will be instantiated upon receiving the reqbody and this will follow the properties defined n the Post model.
        // note: the "key" name of the request from the postman should be similar to the property names defined in the model.
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Retrieve all posts
    @RequestMapping(value="/posts", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Iterable<Post>> getPosts() {

        Iterable<Post> posts = postService.getPosts();
        return ResponseEntity.ok().body(posts);
    }

    // Edit a post
    @RequestMapping(value="/posts/{postId}", method=RequestMethod.PUT)
        // @PathVariable is used for data passed in the URI
            // @RequestParam Vs @PathVariable
            // @RequestParam is used to extract data found in the query param (commonly used for filtering the results based on the condition)
        public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader (value="Authorization") String stringToken, @RequestBody Post post) {

        return postService.updatePost(postId, stringToken, post);
    }

    // Delete a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken) {

        return postService.deletePost(postId, stringToken);
    }


    // Retrieve all user's post
    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Iterable<Post>> getMyPosts(@RequestHeader(value = "Authorization") String stringToken) {
        Iterable<Post> posts = postService.getMyPosts(stringToken);
        return ResponseEntity.ok().body(posts);

    }

}
